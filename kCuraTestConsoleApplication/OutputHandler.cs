using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;

namespace kCuraTestConsoleApplication
{
    public class OutputHandler : IOutputHandler
    {
        public void SaveCitiesByPopulation(List<CityInformation> cities, string directory = null)
        {
            const string fileName = "Cities_By_Population.txt";
            directory = directory ?? Environment.CurrentDirectory;
            var path = Path.Combine(directory, fileName);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            using (var filestream = new FileStream(path, FileMode.CreateNew))
            {
                using (var writer = new StreamWriter(filestream))
                {
                    cities.GroupBy(information => information.Population)
                        .ToList().ForEach(grouping =>
                        {
                            writer.WriteLine(grouping.Key);
                            writer.WriteLine();
                            grouping.ToList().ForEach(information =>
                            {
                                writer.WriteLine($"{information.City}, {information.State}");
                                writer.WriteLine($"Interstates: {string.Join(", ", information.Interstates)}");
                                //todo - this will create and extra line at the end of the file
                                writer.WriteLine();
                            });
                        });
                }
            }
        }

        public void SaveInterstatesByCity(IDictionary<string, int> interstates, string directory = null)
        {
            const string fileName = "Interstates_By_City.txt";
            directory = directory ?? Environment.CurrentDirectory;
            var path = Path.Combine(directory, fileName);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            using (var filestream = new FileStream(path, FileMode.CreateNew))
            {
                using (var writer = new StreamWriter(filestream))
                {
                    interstates
                        .ToList().ForEach(entry =>
                        {
                            writer.WriteLine($"{entry.Key} {entry.Value}");
                        });
                }
            }
        }
    }
}