﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace kCuraTestConsoleApplication
{
    public interface IOutputHandler
    {
        void SaveCitiesByPopulation(List<CityInformation> cities, string directory = null);
        void SaveInterstatesByCity(IDictionary<string,int> interstates, string directory = null);
    }
}