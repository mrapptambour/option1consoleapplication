﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace kCuraTestConsoleApplication
{


    public class ProgramProcessor
    {
        private readonly IOutputHandler _outputHandler;

        public ProgramProcessor(IOutputHandler outputHandler)
        {
            _outputHandler = outputHandler;
        }

        public void Process(Program.Args args)
        {
            if (args.FileLines == null || args.FileLines.Length == 0)
            {
                throw new ArgumentException();
            }

            var directory = Path.GetDirectoryName(args.FilePath);

            var cities = GatherCities(args.FileLines);

            if (args.Option1)
            {
                var citiesByPopulation = cities
                    .Select(CityInformation.SortByInterstateNumber)
                    .OrderByDescending(information => information.Population)
                    .ThenBy(information => information.State)
                    .ThenBy(information => information.City)
                    .ToList();
                _outputHandler.SaveCitiesByPopulation(citiesByPopulation, directory);

                var interstatesByCities = cities
                    .SelectMany(information => information.Interstates)
                    .GroupBy(s => s)
                    .OrderBy(grouping => grouping.Key, new CityInformation.InterstateComparer())
                    .ToDictionary(grouping => grouping.Key, grouping => grouping.Count());

                _outputHandler.SaveInterstatesByCity(interstatesByCities, directory);
            }
        }

        private static IEnumerable<CityInformation> GatherCities(IEnumerable<string> fileLines)
        {
            return fileLines.Select(CityInformation.Parse);
        }
    }

    public class CityInformation
    {
        public string State { get; set; }
        public string City { get; set; }
        public int Population { get; set; }
        public string[] Interstates { get; set; }


        public static CityInformation Parse(string line)
        {
            var info = new CityInformation();
            var elements = line?.Split('|');
            int pop;
            if (int.TryParse(elements?.FirstOrDefault(), out pop))
            {
                info.Population = pop;
            }
            info.City = elements?.Skip(1).FirstOrDefault();
            info.State = elements?.Skip(2).FirstOrDefault();
            info.Interstates = elements?.Skip(3).FirstOrDefault()?.Split(';');
            return info;
        }

        public static CityInformation SortByInterstateNumber(CityInformation arg)
        {
            arg.Interstates = arg.Interstates?.OrderBy(s => s, new InterstateComparer()).ToArray();
            return arg;
        }

        public class InterstateComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                int numx;
                int numy;
                return int.TryParse(x?.Substring(2), out numx) && int.TryParse(y?.Substring(2), out numy)
                    ? numx.CompareTo(numy)
                    : string.Compare(x, y, StringComparison.Ordinal);
            }
        }
    }
}