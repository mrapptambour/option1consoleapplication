﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace kCuraTestConsoleApplication
{
    public class Program
    {
        /// <summary>
        /// ?option1
        /// ?option2
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var programArgs = Args.Parse(args);
            while (string.IsNullOrEmpty(programArgs.FilePath))
            {
                Console.WriteLine("What is the file path?");
                try
                {
                    var path = Console.ReadLine() ?? "";
                    programArgs.FilePath = Path.GetFullPath(path);
                    break;
                }
                catch
                {
                    Console.WriteLine($"File {programArgs.FilePath} cannot be opened. Please try again.");
                    continue;
                }
            }
            while ((programArgs.Option1 || programArgs.Option2) == false)
            {
                Console.WriteLine("Please select an output option by pressing 1 or 2. NOTE - Option 2 is not yet implemented.");
                var key = Console.ReadKey();
                programArgs.Option1 = key.Key == ConsoleKey.D1 || key.Key == ConsoleKey.NumPad1;
                programArgs.Option2 = key.Key == ConsoleKey.D2 || key.Key == ConsoleKey.NumPad2;
                Console.WriteLine();
            }

            try
            {
                var processor = new ProgramProcessor(new OutputHandler());
                programArgs.FileLines = File.ReadAllLines(programArgs.FilePath);
                processor.Process(programArgs);
                Console.WriteLine("Success! Please see the respective output files in the same directory as the input file.");
                Process.Start(Path.GetDirectoryName(programArgs.FilePath));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Sorry, there was a problem processing your file {Environment.NewLine}{ex}");
            }

            Console.WriteLine("Press ESC to quit");
            while (Console.ReadKey().Key != ConsoleKey.Escape)
            {
            }
          
        }

        public class Args
        {
            public string FilePath { get; set; }
            public bool Option1 { get; set; }
            public bool Option2 { get; set; }
            public string[] FileLines { get; set; }
            public static Args Parse(string[] args)
            {
                return new Args()
                {
                    FilePath = args.FirstOrDefault(),
                    Option1 = args.Contains("option1"),
                    Option2 = args.Contains("option2")
                };
            }
        }
    }
}
