### Project Setup

Visual Studio 2015 > Open kCuraTestConsoleApplication.sln

Run or Debug the solution, and following the prompts to first enter the input file path and secondly to choose option "1".

Note - Option 2 has not been developed, but was initially prepared for in the cmd input handling.

Once the kCuraTestConsoleApplication.exe has been deployed, it can be run as follows:

kCuraTestConsoleApplication.exe filepath option1

Example: 
```
kCuraTestConsoleApplication.exe C:\Path\To\Sample_Cities.txt option1
```