using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using kCuraTestConsoleApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kCuraTestUnitTestProject
{
    [TestClass]
    public class OutputHandlerUnitTests
    {
        [TestMethod]
        public void OutputHandler_Option1_Saves_By_Population_Groups()
        {
            var handler = new OutputHandler();
            var cities = new List<CityInformation>()
            {
                new CityInformation()
                {
                    Population = 1,
                    City = "City 1",
                    State = "State 1",
                    Interstates = new string[] {"I-1"}
                },
                new CityInformation()
                {
                    Population = 1,
                    City = "City 2",
                    State = "State 2",
                    Interstates = new string[] {"I-1", "I-2"}
                },
                new CityInformation()
                {
                    Population = 2,
                    City = "City 3",
                    State = "State 3",
                    Interstates = new string[] {"I-3"}
                }
            };
            handler.SaveCitiesByPopulation(cities.OrderByDescending(information => information.Population).ToList());
            var lines = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "Cities_By_Population.txt"));
            Assert.AreEqual("2", lines[0]);
            Assert.IsTrue(string.IsNullOrEmpty(lines[1]));
            Assert.AreEqual("City 3, State 3", lines[2]);
            Assert.AreEqual("Interstates: I-3", lines[3]);
            Assert.IsTrue(string.IsNullOrEmpty(lines[1]));
            Assert.AreEqual("1", lines[5]);
            Assert.IsTrue(string.IsNullOrEmpty(lines[6]));
            Assert.AreEqual("City 1, State 1", lines[7]);
            Assert.AreEqual("Interstates: I-1", lines[8]);
            Assert.IsTrue(string.IsNullOrEmpty(lines[9]));
            Assert.AreEqual("City 2, State 2", lines[10]);
            Assert.AreEqual("Interstates: I-1, I-2", lines[11]);
        }
    }
}