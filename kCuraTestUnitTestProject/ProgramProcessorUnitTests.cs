﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using kCuraTestConsoleApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace kCuraTestUnitTestProject
{
    [TestClass]
    public class ProgramProcessorUnitTests
    {
        [TestMethod]
        [ExpectedException(typeof(Exception), AllowDerivedTypes = true)]
        public void Process_Requires_Lines()
        {
            new ProgramProcessor(Mock.Of<IOutputHandler>()).Process(new Program.Args());
        }

        [TestMethod]
        public void Process_Option1_Sorts_By_Population_High_to_Low_Then_State_Then_City()
        {
            var outputHandler = new Mock<IOutputHandler>();
            outputHandler.Setup(handler => handler.SaveCitiesByPopulation(It.IsAny<List<CityInformation>>(), null))
               .Callback<List<CityInformation>, string>((list, s) =>
               {
                   //Assert
                   //population highest lowest
                   Assert.AreEqual(8, list[0].Population);
                   //then by state
                   Assert.AreEqual("Florida", list[0].State);
                   //then by city
                   Assert.AreEqual("Applesville", list[0].City);
                   Assert.AreEqual("Jacksonville", list[1].City);
                   Assert.AreEqual("Ohio", list[2].State);
                   //lowest population
                   Assert.AreEqual(6, list[3].Population);
               });

            var processor = new ProgramProcessor(outputHandler.Object);
            //Act
            processor.Process(new Program.Args()
            {
                Option1 = true,
                FileLines = new string[]
            {
                    "6|Oklahoma City|Oklahoma|I-35;I-44;I-40",
                    "8|Jacksonville|Florida|I-10;I-95",
                    "8|Columbus|Ohio|I-70;I-71",
                    "8|Applesville|Florida|I-10;I-95"
            }
            });

            outputHandler.Verify(handler => handler.SaveCitiesByPopulation(It.IsAny<List<CityInformation>>(), null), Times.Once);

        }

        [TestMethod]
        public void Process_Option1_Saves_Sorted_Interstates_By_City_By_Number_Ascending()
        {
            var outputHandler = new Mock<IOutputHandler>();
            outputHandler.Setup(handler => handler.SaveInterstatesByCity(It.IsAny<IDictionary<string, int>>(), null))
               .Callback<IDictionary<string, int>, string>((dict, dir) =>
               {
                   Assert.AreEqual(2, dict["I-95"]);
                   Assert.AreEqual(2, dict["I-10"]);
                   Assert.AreEqual("I-10", dict.First().Key);
                   Assert.AreEqual("I-95", dict.Last().Key);
               });

            var processor = new ProgramProcessor(outputHandler.Object);
            //Act
            processor.Process(new Program.Args()
            {
                Option1 = true,
                FileLines = new string[]
                {
                        "6|Oklahoma City|Oklahoma|I-35;I-44;I-40",
                        "8|Jacksonville|Florida|I-10;I-95",
                        "8|Columbus|Ohio|I-70;I-71",
                        "8|Applesville|Florida|I-10;I-95"
                }
            });

            outputHandler.Verify(handler => handler.SaveInterstatesByCity(It.IsAny<IDictionary<string, int>>(), null), Times.Once);

        }

        [TestMethod]
        public void Process_Option1_Sorts_Interstates_For_Each_City()
        {
            var outputHandler = new Mock<IOutputHandler>();
            outputHandler.Setup(handler => handler.SaveCitiesByPopulation(It.IsAny<List<CityInformation>>(), null))
               .Callback<List<CityInformation>, string>((list, s) =>
               {
                   //Assert
                   //sorted by interstate number ascending
                   //All interstates have the “I-” prefix.
                   Assert.AreEqual("I-35", list[0].Interstates[0]);
                   Assert.AreEqual("I-40", list[0].Interstates[1]);
                   Assert.AreEqual("I-44", list[0].Interstates[2]);
               });

            var processor = new ProgramProcessor(outputHandler.Object);
            //Act
            processor.Process(new Program.Args()
            {
                Option1 = true,
                FileLines = new string[]
            {
                    "6|Oklahoma City|Oklahoma|I-35;I-44;I-40;I-250"
            }
            });

            outputHandler.Verify(handler => handler.SaveCitiesByPopulation(It.IsAny<List<CityInformation>>(), null), Times.Once);

        }

        [TestMethod]
        public void Process_Option1_Passes_The_Directory_Of_The_Source_File()
        {
            var outputHandler = new Mock<IOutputHandler>();
            outputHandler.Setup(handler => handler.SaveCitiesByPopulation(It.IsAny<List<CityInformation>>(), It.IsAny<string>())).Verifiable();
            outputHandler.Setup(handler => handler.SaveInterstatesByCity(It.IsAny<IDictionary<string, int>>(), It.IsAny<string>())).Verifiable();

            var processor = new ProgramProcessor(outputHandler.Object);
            processor.Process(new Program.Args()
            {
                Option1 = true,
                FilePath = Environment.CurrentDirectory,
                FileLines = new string[]
            {
                    "6|Oklahoma City|Oklahoma|I-35;I-44;I-40",
                    "8|Jacksonville|Florida|I-10;I-95",
                    "8|Columbus|Ohio|I-70;I-71",
                    "8|Applesville|Florida|I-10;I-95"
            }
            });

            outputHandler.Verify(handler => handler.SaveCitiesByPopulation(It.IsAny<List<CityInformation>>(), Path.GetDirectoryName(Environment.CurrentDirectory)), Times.Once);
            outputHandler.Verify(handler => handler.SaveInterstatesByCity(It.IsAny<IDictionary<string, int>>(), Path.GetDirectoryName(Environment.CurrentDirectory)), Times.Once);
        }

        [TestMethod]
        public void CityInformation_Parses_Line_Info()
        {
            var info = CityInformation.Parse("6|Oklahoma City|Oklahoma|I-35;I-44;I-40");
            Assert.AreEqual(6, info.Population);
            Assert.AreEqual("Oklahoma City", info.City);
            Assert.AreEqual("Oklahoma", info.State);
            Assert.AreEqual(3, info.Interstates.Length);
            Assert.AreEqual("I-40", info.Interstates.Last());
        }
    }
}
